from optparse import OptionParser


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("--hidden", type="int", help="Number of hidden neural nets.")
