import os
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

DATA_FOLDER = os.environ.get("DATA_FOLDER")
CONF_FOLDER = os.environ.get("CONF_FOLDER")
RAW_DATA = os.environ.get("RAW_DATA")
DATASETS = os.environ.get("DATASETS")

GENERIC_FILENAME = os.environ.get("GENERIC_FILENAME")

PROJECT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
DATA_PATH = os.path.join(PROJECT_DIR, DATA_FOLDER)


class DataFilesConf:

    class Paths:
        data = DATA_PATH
        conf = join(PROJECT_DIR, CONF_FOLDER)
        raw_data = join(DATA_PATH, RAW_DATA)
        datasets = join(DATA_PATH, DATASETS)

    class FileNames:
        generic_filename = join(DATA_PATH, GENERIC_FILENAME)

