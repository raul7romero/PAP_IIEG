from conf.settings import DataFilesConf
import os


def create_dir(reference_path):
    if not os.path.exists(reference_path):
        os.mkdir(reference_path)


def create_dirs():
    create_dir(DataFilesConf.Paths.data)
    create_dir(DataFilesConf.Paths.raw_data)
    create_dir(DataFilesConf.Paths.datasets)


if __name__ == "__main__":
    create_dirs()
