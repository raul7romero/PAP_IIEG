# IIEG PAP

This is the main repo for IIEG PAP - ITESO 2017.

Contributors:
* Alicia Karime Gonzalez
* Rodrigo Hern'andez Mota
* Ra'ul Romero Barrag'an

## Usage

Run the following in the command line: 

* ```source activate venv```
* ```python main.py```

## Setup

1. Create your `.env` file.

* ```cp conf/.env.example .env```

2. Go to the root directory and create a virtual-environment.

* ```virtualenv venv```
* ```source activate venv```

3. Install requirements

* ```pip install -r requirements.txt```

4. Run setup file.

* ```python setup.py```

## Repository Structure

```
|- conf/
|   |- .env.example
|   |- settings.py
|
|- data/
|   |- datasets/
|   |- raw_data/
|
|- util/
|   | ...
|
|- venv/
|
|- main.py
|- README.md
|- .gitignore
```

## Todo

Bugs and to-do stuff:

1. [add todo]
2. [add todo]
